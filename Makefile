PROJ_NAME=control_pad
SRCS = main.c stm8l15x_it.c stm8_interrupt_vector.c controller_driver.c engine.c renderer.c
DEPS_SRCS = stm8l15x_clk.c stm8l15x_gpio.c stm8l15x_tim4.c stm8l15x_usart.c stm8l15x_dma.c stm8l15x_adc.c stm8l15x_flash.c

SDCC_PATH=../../sdcc-3.4.0/bin
STM8S_LIBS_PATH=../stm8l_libs
SDCC=$(SDCC_PATH)/sdcc
SDLD=$(SDCC_PATH)/sdld
DEPS_OBJ = $(DEPS_SRCS:.c=.rel)
OBJS = $(SRCS:.c=.rel)


CFLAGS=-I. -I$(STM8S_LIBS_PATH)/inc -D__SDCC_STM8L__ -DSTM8L15X_MD  --opt-code-speed 

.PHONY: all clean flash

all: build


build: $(DEPS_OBJ) $(OBJS) 
	$(SDCC) -lstm8 -mstm8 --out-fmt-ihx $(CFLAGS) $(LDFLAGS) $(OBJS) $(DEPS_OBJ) -o $(PROJ_NAME).ihx

clean:
	rm -f $(PROJ_NAME).ihx *.lk  *.lst  *.map  *.rel  *.rst  *.sym *.asm *.adb

flash: clean all
	stm8flash -s flash -c stlinkv2 -p stm8l150 -w  $(PROJ_NAME).ihx


%.rel: $(STM8S_LIBS_PATH)/src/%.c 
	$(SDCC) -lstm8 -mstm8 $(CFLAGS) $(LDFLAGS) -c $<

%.rel: %.c 
	$(SDCC) -lstm8 -mstm8 $(CFLAGS) $(LDFLAGS) -c $<
