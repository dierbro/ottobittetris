#include "renderer.h"
#include "controller_driver.h"
#include "engine.h"
#include "stdio.h"
#include "string.h"

uint8_t currentStatus = PLAYFIELD_TH;

void render(void){
    //GPIO_ToggleBits(GPIOE, GPIO_Pin_7);
    memset(framebuffer1, '\0', 256);
    switch(currentGameStatus){
        case RUNNING:
            switch((MachineStatus)currentStatus){
                case PLAYFIELD_TH:
                    renderPlayfield();
                    currentStatus = PLAYFIELD_BH;
                    break;
                case PLAYFIELD_BH:
                    renderPlayfield();
                    currentStatus = SCOREBOARD;
                    break;
                case SCOREBOARD:
                    renderScoreBoard();
                    currentStatus = PLAYFIELD_TH;
                    break;
            }
            break;
        case GAMEOVER:
            sprintf(framebuffer1,"\033[H\033[10;0H"); 
            sprintf(framebuffer1,"\033[30CGAMEOVERi\n\r"); 
            sprintf(framebuffer1,"\033[30C Press START to restart the game"); 
            flushUSARTTXBuffer();
            break;
        case PAUSED:
            sprintf(framebuffer1,"\033[H\033[10;0H"); 
            sprintf(framebuffer1,"\033[30CPAUSED\n\r"); 
            sprintf(framebuffer1,"\033[30C Press START to restart the game"); 
            flushUSARTTXBuffer();
            break;
    }

}

void renderPlayfield(){
    uint8_t r = 0, c = 0, maxHeight = MAXHEIGHT;
    uint16_t line = 0;


    if((MachineStatus)currentStatus == PLAYFIELD_TH){
        maxHeight = MAXHEIGHT/2;
        sprintf(framebuffer1,"\033[H\033[?25l"); 
    }else{
        r = MAXHEIGHT/2;
        sprintf(framebuffer1,"\033[%d;0H", r+1); 
    }

    for(; r<maxHeight; r++){
        line = playfield[r];
        if( r >= currentTetromino.y && r < currentTetromino.y+4){
            for(c = 0; c<4; c++){
                if(CURRENT_TETROMINO & 1<<(((r-currentTetromino.y)*4)+c)){
                    SetBit16(line,currentTetromino.x+c); 
                }
            }
        }
        sprintf(framebuffer1+strlen(framebuffer1),ROW_PATTERN, ROW_TO_BINARY(line)); 
    }
    if((MachineStatus)currentStatus == PLAYFIELD_BH)
        sprintf(framebuffer1+strlen(framebuffer1),"\033[30C<!==========!>\n\r"); 
    flushUSARTTXBuffer();
}

void renderScoreBoard(){
    sprintf(framebuffer1,"\033[5;0H"); 
    sprintf(framebuffer1+strlen(framebuffer1),"\033[5CPLAYER: Diego\n\r"); 
    sprintf(framebuffer1+strlen(framebuffer1),"\033[5CSCORE: %d\n\r", score); 
    sprintf(framebuffer1+strlen(framebuffer1),"\033[5CLEVEL: 1\n\r"); 
    sprintf(framebuffer1+strlen(framebuffer1),"\033[5CLEVEL: %d\n\r", FLASH->IAPSR); 
    flushUSARTTXBuffer();
}
