/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __RENDERER_H
#define __RENDERER_H


/* Includes ------------------------------------------------------------------*/
#include "engine.h"
extern uint8_t framebuffer1[]; 
typedef enum {PLAYFIELD_TH = 0, PLAYFIELD_BH = 1, SCOREBOARD = 2} MachineStatus;
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
#define ROW_PATTERN "\033[30C<!%s%s%s%s%s%s%s%s%s%s!>\n\r"

#define ROW_TO_BINARY(byte)  \
  (byte & 0x10 ? "#" : "."), \
  (byte & 0x20 ? "#" : "."), \
  (byte & 0x40 ? "#" : "."), \
  (byte & 0x80 ? "#" : "."), \
  (byte & 0x0100 ? "#" : "."), \
  (byte & 0x0200 ? "#" : "."), \
  (byte & 0x0400 ? "#" : "."), \
  (byte & 0x0800 ? "#" : "."), \
  (byte & 0x1000 ? "#" : "."), \
  (byte & 0x2000 ? "#" : ".")

/* Exported functions ------------------------------------------------------- */
void render(void); 
void cleanScrean(void);
void renderPlayfield(void);
void renderCurrentTetromino(void);
void renderScoreBoard(void);
void flushUSARTTXBuffer(void);

#endif /* __CONTROLLER_DRIVER_H */


