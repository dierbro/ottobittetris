/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ENGINE_H
#define __ENGINE_H


/* Includes ------------------------------------------------------------------*/
#include "stm8l15x.h"
#include "engine.h"

#define MAXWIDTH 10
#define MAXHEIGHT 20

#define TICSPERSECONDS 50
#define PAST_TETROMINO pastTetromino.schema[pastTetromino.rotation]
#define CURRENT_TETROMINO currentTetromino.schema[currentTetromino.rotation]
#define SetBit16(VAR,Place)         ( (VAR) |= (uint16_t)((uint16_t)1<<(uint16_t)(Place)) )
#define ClrBit16(VAR,Place)         ( (VAR) &= (uint16_t)((uint16_t)((uint16_t)1<<(uint16_t)(Place))^(uint16_t)65535) )
#define ValBit16(VAR,Place)         ((uint16_t)(VAR) & (uint16_t)((uint16_t)1<<(uint16_t)(Place)))

typedef uint16_t TetrominoScheme;
typedef struct tetromino{
    TetrominoScheme schema[4];
    uint8_t rotation;
    uint8_t x;
    uint8_t y;
    bool doNotClean;
} Tetromino;

typedef enum {RUNNING = 0, GAMEOVER = 1, PAUSED = 2} GameStatuses;

/* Exported constants --------------------------------------------------------*/
// tetromino data
const static TetrominoScheme TETRA_I[4] = {0x0F00, 0x2222, 0x00F0, 0x4444};

const static TetrominoScheme TETRA_J[4] = {0x8E00, 0x6440, 0x0E20, 0x44C0};

const static TetrominoScheme TETRA_L[4] = {0x2E00, 0x4460, 0x0E80, 0xC440};

const static TetrominoScheme TETRA_O[4] = {0x6600, 0x6600, 0x6600, 0x6600};

const static TetrominoScheme TETRA_S[4] = {0x6C00, 0x4620, 0x06C0, 0x8c40};

const static TetrominoScheme TETRA_T[4] = {0x4E00, 0x4640, 0x0E40, 0x4C40};

const static TetrominoScheme TETRA_Z[4] = {0xC600, 0x2640, 0x0C60, 0x4C80};

extern Tetromino currentTetromino;
extern Tetromino pastTetromino;
extern uint16_t score;
extern uint16_t playfield[];
extern GameStatuses currentGameStatus;
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void initEngine(void); 
void updateEngine(void); 
void scrollTetromino(void);
void createNewTetromino(void);
void pickRandomTetromino(void);
void copyTetromino(void);
void updatePlayfield(void);
void resetTetromino(void);
bool validMove(void);
void lockTetromino(void);
void pauseStartGame(void);
void pauseGame(void);

#endif /* __CONTROLLER_DRIVER_H */



