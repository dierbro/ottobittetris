/* Includes ------------------------------------------------------------------*/
#include "controller_driver.h"
#include "stdio.h"

uint8_t KEY_Status = 255; // KEY_UP | KEY_DOWN | KEY_RIGHT | KEY_LEFT | KEY_A | KEY_B | KEY_C | KEY_START
uint8_t KEY_PRESSED_Status = 0; // KEY_UP | KEY_DOWN | KEY_RIGHT | KEY_LEFT | KEY_A | KEY_B | KEY_C | KEY_START
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Public functions ----------------------------------------------------------*/
void MegaDriveControlPad_Read(BitStatus s){
    /*
     * Select signal - GPIOB pin6
     * CH0 UP/UP - GPIOB pin0
     * CH1 DOWN/DOWN - GPIOB pin1
     * CH2 LEFT/GND - GPIOB pin2
     * CH3 RIGHT/GND - GPIOB pin3
     * CH4 B/A - GPIOB pin4
     * CH6 C/START - GPIOB pin5
     */
    uint8_t OldKeyStatus = KEY_Status;
    if(s){
        MskBit(KEY_Status, 0x3F, (uint8_t) GPIO_ReadInputData(GPIOB));
        MskBit(KEY_PRESSED_Status, 0x3F, (uint8_t) ((OldKeyStatus ^ KEY_Status) & KEY_Status));
    }else{
        MskBit(KEY_Status, 0xC0, (uint8_t) GPIO_ReadInputData(GPIOB)<<2);
        MskBit(KEY_PRESSED_Status, 0xC0, (uint8_t) ((OldKeyStatus ^ KEY_Status) & KEY_Status));
    }


    return;
} 


