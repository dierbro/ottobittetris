#include "engine.h"
#include "controller_driver.h"
#include "string.h"
#include "stdlib.h"

Tetromino currentTetromino;
Tetromino pastTetromino;
uint16_t playfield[MAXHEIGHT];
GameStatuses currentGameStatus;

uint8_t secondsTimer = 0; 
uint8_t keyPressTimer = 0; 
uint16_t score = 0;
void initEngine(){
    uint8_t i;
    
    for(i=0; i< MAXHEIGHT; i++){
        playfield[i] = 0;
    }
    currentGameStatus = RUNNING;

    createNewTetromino();
}

void updateEngine(void){
    bool goingDown = FALSE;
    
    if(KEY_START_PRESSED!=0){
        pauseStartGame();
    }
    
    if(currentGameStatus != RUNNING )
        return;
    
    copyTetromino();

    if(KEY_A_PRESSED!=0 || KEY_UP_PRESSED != 0){
        currentTetromino.rotation++;
        currentTetromino.rotation %= 4;
    }
    
    if(KEY_DOWN_PRESSED!=0 || (KEY_DOWN == 0 && keyPressTimer++>10)){
        keyPressTimer = 0;
        goingDown = TRUE;
        scrollTetromino();
    }

    if(KEY_B_PRESSED != 0){
        while(validMove()){
            currentTetromino.y++;
        }
        currentTetromino.y--;
    }
    
    if(KEY_RIGHT_PRESSED!=0){
        currentTetromino.x++;
    }
    
    if(KEY_LEFT_PRESSED!=0){
        currentTetromino.x--;
    }
    

    if(++secondsTimer%TICSPERSECONDS == 0){
        goingDown = TRUE;
        scrollTetromino();
    }

    if(!validMove()){
        if(goingDown==TRUE){
            lockTetromino();
        }
        else{
            resetTetromino();
        }
    }
}

void pauseStartGame(){
    switch(currentGameStatus){
        case RUNNING:
            pauseGame();
            currentGameStatus = PAUSED;
            break;
        case GAMEOVER:
            initEngine();
            break;
        case PAUSED:
            currentGameStatus = RUNNING;
    }
}

void pauseGame(){

    uint32_t Address = FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS;
    const char * s;
    size_t acount= 16;

    FLASH_DeInit();

    FLASH_Unlock(FLASH_MemType_Data);

    // Wait for unlock
    while ((FLASH->IAPSR  & (uint8_t)FLASH_FLAG_DUL) == RESET);

    s = (const void *)TETRA_L;

    while(acount--)
        *(NEAR uint8_t*) (uint16_t)Address++ = (uint8_t)*s++;


    FLASH_Lock(FLASH_MemType_Data);
}

bool validMove(){
    uint8_t r = 0, c = 0;

    for(r = 0; r<4; r++){
        for(c = 0; c<4; c++){
            if(CURRENT_TETROMINO & 1<<((r*4)+c)){
                if((currentTetromino.y+r < 0) || (currentTetromino.y+r >= MAXHEIGHT) || ( currentTetromino.x+c < 4) || ( currentTetromino.x+c >= MAXWIDTH+4)) {
                    return FALSE;
                }
                if(ValBit16(playfield[currentTetromino.y+r], currentTetromino.x+c)!=0){
                    return FALSE;
                }
            }
        }
    };

    return TRUE;
}

void scrollTetromino(void){
    currentTetromino.y++;
    
    secondsTimer = 0;
}

void lockTetromino(){
    uint8_t r, c;
    bool goodToGo = FALSE;
    uint8_t completed_lines = 0;

    for(r = 0; r<4; r++){
        for(c = 0; c<4; c++){
            if(PAST_TETROMINO & 1<<((r*4)+c)){
                 SetBit16(playfield[pastTetromino.y+r], pastTetromino.x+c); 
            }
        }
    }

    while(goodToGo == FALSE){
        goodToGo = TRUE;
        for(r = MAXHEIGHT-1; r > 0; r--){
            // 16368 == 0011111111110000
            if(playfield[r] == 16368){
                completed_lines++;
                goodToGo  = FALSE;
                for ( c = r  ; c > 1 ; c-- ){ 
                    playfield[c] = playfield[c-1];
                }
            }
        }
    }

    if(completed_lines > 0) {
        // tetris
        score += completed_lines/4 * 800;
        completed_lines = completed_lines % 4;

        // triple
        score += completed_lines/3 * 500;
        completed_lines = completed_lines % 3;

        // double
        score += completed_lines/2 * 300;
        completed_lines = completed_lines % 2;

        // single
        score += completed_lines * 100;
    }

    createNewTetromino();
    if(!validMove()){
        currentGameStatus = GAMEOVER;
    }
}

void createNewTetromino(void){
    pastTetromino.doNotClean = TRUE;
    pickRandomTetromino();
    currentTetromino.y=0;
    currentTetromino.x=rand()%6+4;
    currentTetromino.rotation = rand()%4;
}

void pickRandomTetromino(void){
    switch(rand()%7) {
        case 0:
            memcpy(currentTetromino.schema, TETRA_I, sizeof(TetrominoScheme)*4);
            break;
        case 1:
            memcpy(currentTetromino.schema, TETRA_J, sizeof(TetrominoScheme)*4);
            break;
        case 2:
            memcpy(currentTetromino.schema, TETRA_L, sizeof(TetrominoScheme)*4);
            break;
        case 3:
            memcpy(currentTetromino.schema, TETRA_O, sizeof(TetrominoScheme)*4);
            break;
        case 4:
            memcpy(currentTetromino.schema, TETRA_S, sizeof(TetrominoScheme)*4);
            break;
        case 5:
            memcpy(currentTetromino.schema, TETRA_T, sizeof(TetrominoScheme)*4);
            break;
        case 6:
            memcpy(currentTetromino.schema, TETRA_Z, sizeof(TetrominoScheme)*4);
            break;
    }
}

void copyTetromino(){
    pastTetromino.y = currentTetromino.y;
    pastTetromino.x = currentTetromino.x;
    pastTetromino.rotation = currentTetromino.rotation;
    memcpy(pastTetromino.schema, currentTetromino.schema, sizeof(TetrominoScheme)*4);
}

void resetTetromino(){
    currentTetromino.y = pastTetromino.y;
    currentTetromino.x = pastTetromino.x;
    currentTetromino.rotation = pastTetromino.rotation;
    memcpy(currentTetromino.schema, pastTetromino.schema, sizeof(TetrominoScheme)*4);
}

