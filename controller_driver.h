/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CONTROLLER_DRIVER_H
#define __CONTROLLER_DRIVER_H


/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_gpio.h"
//#define SET_KEY_UP(x) (KEY_Status ^= (-x ^ KEY_Status) & (1 << 7))
//#define SET_KEY_DOWN(x) (KEY_Status ^= (-x ^ KEY_Status) & (1 << 6)) 
//#define SET_KEY_RIGHT(x) (KEY_Status ^= (-x ^ KEY_Status) & (1 << 5)) 
//#define SET_KEY_LEFT(x) (KEY_Status ^= (-x ^ KEY_Status) & (1 << 4)) 
#define SET_KEY_A(x) (KEY_Status ^= (-x ^ KEY_Status) & (1 << 6)) 
//#define SET_KEY_B(x) (KEY_Status ^= (-x ^ KEY_Status) & (1 << 2)) 
//#define SET_KEY_C(x) (KEY_Status ^= (-x ^ KEY_Status) & (1 << 1)) 
#define SET_KEY_START(x) (KEY_Status ^= (-x ^ KEY_Status) & (1 << 7)) 

#define KEY_UP (ValBit(KEY_Status, 0)) 
#define KEY_DOWN (ValBit(KEY_Status, 1))
#define KEY_LEFT (ValBit(KEY_Status, 2))
#define KEY_RIGHT (ValBit(KEY_Status, 3))
#define KEY_A (ValBit(KEY_Status, 6))
#define KEY_B (ValBit(KEY_Status, 4))
#define KEY_C (ValBit(KEY_Status, 5))
#define KEY_START (ValBit(KEY_Status, 7))

#define KEY_UP_PRESSED (ValBit(KEY_PRESSED_Status, 0)) 
#define KEY_DOWN_PRESSED (ValBit(KEY_PRESSED_Status, 1))
#define KEY_LEFT_PRESSED (ValBit(KEY_PRESSED_Status, 2))
#define KEY_RIGHT_PRESSED (ValBit(KEY_PRESSED_Status, 3))
#define KEY_A_PRESSED (ValBit(KEY_PRESSED_Status, 6))
#define KEY_B_PRESSED (ValBit(KEY_PRESSED_Status, 4))
#define KEY_C_PRESSED (ValBit(KEY_PRESSED_Status, 5))
#define KEY_START_PRESSED (ValBit(KEY_PRESSED_Status, 7))
/* Exported types ------------------------------------------------------------*/
typedef enum {PRESSED = 0, RELEASED = !ERROR} KeyStatus;
typedef struct KEY_struct{
    __I uint8_t status; // KEY_UP | KEY_DOWN | KEY_LEFT | KEY_RIGHT | KEY_A | KEY_B | KEY_C | KEY_START
}
KEY_TypeDef;
extern uint8_t KEY_Status; // KEY_UP | KEY_DOWN | KEY_RIGHT | KEY_LEFT | KEY_A | KEY_B | KEY_C | KEY_START
extern uint8_t KEY_PRESSED_Status; // KEY_UP | KEY_DOWN | KEY_RIGHT | KEY_LEFT | KEY_A | KEY_B | KEY_C | KEY_START

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void MegaDriveControlPad_Read(BitStatus s); 

#endif /* __CONTROLLER_DRIVER_H */

