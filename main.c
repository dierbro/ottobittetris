/* Includes ------------------------------------------------------------------*/
#include "stm8l15x.h"
#include "stm8l15x_it.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "renderer.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define TIM4_PERIOD       18
//#define USART_DMA_CHANNEL_TX2   DMA1_Channel3
#define USART_DMA_CHANNEL_TX1   DMA1_Channel1
//#define USART_DMA_FLAG_TCTX2    (uint16_t)DMA1_FLAG_TC2
#define USART_DMA_FLAG_TCTX1    (uint16_t)DMA1_FLAG_TC1
#define USART_DR_ADDRESS       (uint16_t)0x5231 
#define USART_BAUDRATE         (uint32_t)115200
//#define USART_BAUDRATE         (uint32_t)921600
#define DATA_TO_TRANSFER       (countof(framebuffer1) - 1)
/* define the GPIO port and pins connected to Leds mounted on STM8L152X-EVAL board */
/* Private macro -------------------------------------------------------------*/
#define countof(a)            (sizeof(a) / sizeof(*(a)))
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
uint8_t framebuffer1[256]; 
//uint8_t framebuffer2[256]; 
void Setup_Clock(void)
{
    /* High speed internal clock prescaler: 1 */
    CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);
    
    CLK_PeripheralClockConfig(CLK_Peripheral_DMA1, ENABLE);

    /* Select HSI as system clock source */
    CLK_SYSCLKSourceSwitchCmd(ENABLE);
    CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
    while (CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSI){}
}

void Setup_GPIO(void)
{
    /* Setup GPIO */
    GPIO_DeInit(GPIOB);
    /* Controller signals */
    GPIO_Init(GPIOB, GPIO_Pin_0, GPIO_Mode_In_FL_No_IT);
    GPIO_Init(GPIOB, GPIO_Pin_1, GPIO_Mode_In_FL_No_IT);
    GPIO_Init(GPIOB, GPIO_Pin_2, GPIO_Mode_In_FL_No_IT);
    GPIO_Init(GPIOB, GPIO_Pin_3, GPIO_Mode_In_FL_No_IT);
    GPIO_Init(GPIOB, GPIO_Pin_4, GPIO_Mode_In_FL_No_IT);
    GPIO_Init(GPIOB, GPIO_Pin_5, GPIO_Mode_In_FL_No_IT);
    /* Controller Select */
    GPIO_Init(GPIOB, GPIO_Pin_6, GPIO_Mode_Out_PP_Low_Fast);
    
    /* Setup LEDs */
    GPIO_Init(GPIOE, GPIO_Pin_7, GPIO_Mode_Out_PP_Low_Fast);
}

void Setup_Timer4(void)
{
    /* Enable TIM4 CLK */
    CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);

     /* Time base configuration */
    TIM4_TimeBaseInit(TIM4_Prescaler_8192, TIM4_PERIOD);
    /* Clear TIM4 update flag */
    TIM4_ClearFlag(TIM4_FLAG_Update);
    /* Enable update interrupt */
    TIM4_ITConfig(TIM4_IT_Update, ENABLE);
}

void Setup_DMA(void){
    //http://www.st.com/web/en/resource/technical/document/application_note/CD00258160.pdf
    /* Deinitialize DMA channels */
    DMA_GlobalDeInit();

    DMA_DeInit(USART_DMA_CHANNEL_TX1);

}

void flushUSARTTXBuffer(){
    DMA_Init(USART_DMA_CHANNEL_TX1, (uint16_t)framebuffer1, (uint16_t)USART_DR_ADDRESS, 
            (uint8_t)DATA_TO_TRANSFER, DMA_DIR_MemoryToPeripheral, DMA_Mode_Normal,
            DMA_MemoryIncMode_Inc, DMA_Priority_High, DMA_MemoryDataSize_Byte);
    /* Enable the USART Tx/Rx DMA requests */
    USART_DMACmd(USART1, USART_DMAReq_TX, ENABLE);
   
    // Enable interrupts
    DMA_ITConfig(DMA1_Channel1, DMA_ITx_TC, ENABLE);

    /* Global DMA Enable */
    DMA_GlobalCmd(ENABLE);
    /* Enable the USART Tx DMA channel */
    DMA_Cmd(USART_DMA_CHANNEL_TX1, ENABLE);

    // while (DMA_GetFlagStatus((DMA_FLAG_TypeDef)USART_DMA_FLAG_TCTX1) == RESET){  };

}

void Setup_USART1(uint32_t USART_BaudRate, 
        USART_WordLength_TypeDef USART_WordLength,
        USART_StopBits_TypeDef USART_StopBits,
        USART_Parity_TypeDef USART_Parity,
        USART_Mode_TypeDef USART_Mode)
{
    /* Enable USART clock */
    CLK_PeripheralClockConfig(CLK_Peripheral_USART1, ENABLE);

    /* Configure USART Tx as alternate function push-pull  (software pull up)*/
    GPIO_ExternalPullUpConfig(GPIOC, GPIO_Pin_3, ENABLE);

    /* Configure USART Rx as alternate function push-pull  (software pull up)*/
    GPIO_ExternalPullUpConfig(GPIOC, GPIO_Pin_2, ENABLE);

    /* USART configuration */
    USART_Init(USART1, USART_BaudRate,
            USART_WordLength,
            USART_StopBits,
            USART_Parity,
            USART_Mode);

    USART_Cmd(USART1, DISABLE);
}

void initSeed(){
    // http://www.zilogic.com/blog/tutorial-random-numbers.html
    uint16_t ADCData;
    ADC_DeInit(ADC1);
    CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, ENABLE);

    ADC_Cmd(ADC1, ENABLE);
    ADC_Init(ADC1, ADC_ConversionMode_Single, ADC_Resolution_12Bit, ADC_Prescaler_2);
    ADC_SamplingTimeConfig(ADC1, ADC_Group_FastChannels, ADC_SamplingTime_9Cycles);
    ADC_ChannelCmd(ADC1, ADC_Channel_3, ENABLE);

    ADC_SoftwareStartConv(ADC1);
    /* Wait until ADC end of conversion */
    while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == 0) {}
    ADCData = ADC_GetConversionValue(ADC1);

    srand(ADCData);

    ADC_DeInit(ADC1);
    CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, DISABLE);
    ADC_ChannelCmd(ADC1, ADC_Channel_3, DISABLE);
    ADC_Cmd(ADC1, DISABLE);
}

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
void main(void)
{
    Setup_Clock();

    initSeed();
    Setup_GPIO();

    Setup_Timer4();
    
    Setup_USART1(USART_BAUDRATE, USART_WordLength_8b, USART_StopBits_1,
            USART_Parity_No, (USART_Mode_TypeDef)(USART_Mode_Tx | USART_Mode_Rx));
    
    Setup_DMA(); 
    USART_Cmd(USART1, ENABLE);

    initEngine();

     
    /* enable interrupts */
    enableInterrupts();
    /* Enable TIM4 */
    /* 16ms */
    TIM4_Cmd(ENABLE);
    
    sprintf(framebuffer1,"\033[H\033[J\033[?25l"); 
    sprintf(framebuffer1+strlen(framebuffer1),"OTTObit TETRIS"); 
    flushUSARTTXBuffer();
     

  while (1)
  {
    wfi();
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* Infinite loop */
  while (1)
  {}
}
#endif

